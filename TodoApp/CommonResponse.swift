//
//  CommonResponse.swift
//  TodoApp
//
//  Created by rogiso on 2020/06/18.
//  Copyright © 2020 rogiso. All rights reserved.
//

import Foundation

struct CommonResponse: BaseResponse {
    var errorCode: Int
    var errorMessage: String
}
