//
//  TodoDeleteRequest.swift
//  TodoApp
//
//  Created by rogiso on 2020/07/10.
//  Copyright © 2020 rogiso. All rights reserved.
//

import Alamofire

struct TodoDeleteRequest: RequestProtocol {
    typealias Response = CommonResponse

    let id: Int

    var parameters: Parameters? {
        return nil
    }

    var path: String {
        return "/todos/\(id)"
    }

    var method: HTTPMethod {
        return .delete
    }
}
