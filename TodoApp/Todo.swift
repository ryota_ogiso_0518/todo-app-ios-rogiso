//
//  Todo.swift
//  TodoApp
//
//  Created by rogiso on 2020/06/15.
//  Copyright © 2020 rogiso. All rights reserved.
//

import Foundation

struct Todo: Codable {
    let id: Int
    var title: String
    var detail: String?
    var date: Date?
}
