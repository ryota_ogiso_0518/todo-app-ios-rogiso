//
//  TodoListViewController.swift
//  TodoApp
//
//  Created by rogiso on 2020/06/05.
//  Copyright © 2020 rogiso. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialSnackbar

final class TodoListViewController: UIViewController {
    private var isDeleteMode = false
    private var composeBarButtonItem: UIBarButtonItem!
    private var todos: [Todo] = []
    @IBOutlet private weak var todoTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigationItem()
        todoTableView.dataSource = self
        todoTableView.delegate = self
        fetchTodoList()
    }
    
    private func fetchTodoList() {
        APIClient().call(
            request: TodosGetRequest(),
            success: { [weak self] result in
                // 取得成功時の処理
                self?.todos = result.todos
                self?.todoTableView.reloadData()
            },
            failure: { [weak self] error in
                // 取得失敗時の処理
                self?.showErrorAlert(message: error)
            }
        )
    }

    private func deleteTodo(id: Int) {
        APIClient().call(
            request: TodoDeleteRequest(id: id),
            success: { [weak self] _ in
                self?.fetchTodoList()
            },
            failure: { [weak self] message in
                self?.showErrorAlert(message: message)
            }
        )
    }

    private func setUpNavigationItem() {
        navigationItem.title = "TODO APP"
        composeBarButtonItem = UIBarButtonItem(barButtonSystemItem: .compose, target: self, action: #selector(composeBarButtonPushed(_:)))
        let trashBarButtonItem = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(trashBarButtonPushed(_:)))
        navigationItem.rightBarButtonItems = [trashBarButtonItem, composeBarButtonItem]
        navigationItem.backBarButtonItem = UIBarButtonItem()
    }

    @objc private func composeBarButtonPushed(_ sender: UIBarButtonItem) {
        isDeleteMode = false
        updateTrashBarButtonItem()
        transitionToTodoEditView()
    }

    @objc private func trashBarButtonPushed(_ sender: UIBarButtonItem) {
        isDeleteMode.toggle()
        updateTrashBarButtonItem()
    }

    private func updateTrashBarButtonItem() {
        let systemItem: UIBarButtonItem.SystemItem = isDeleteMode ? .stop : .trash
        let trashBarButtonItem = UIBarButtonItem(barButtonSystemItem: systemItem, target: self, action: #selector(trashBarButtonPushed(_:)))
        navigationItem.rightBarButtonItems = [trashBarButtonItem, composeBarButtonItem]
    }

    private func showSnackbar(text: String) {
        let message = MDCSnackbarMessage()
        message.text = text
        MDCSnackbarManager.show(message)
    }

    private func transitionToTodoEditView(todo: Todo? = nil) {
        let storyboard = UIStoryboard(name: "Edit", bundle: nil)
        guard let todoEditViewController = storyboard.instantiateInitialViewController() as? TodoEditViewController else { return }
        todoEditViewController.delegate = self
        todoEditViewController.todo = todo
        navigationController?.pushViewController(todoEditViewController, animated: true)
    }

    private func showDeleteAlert(todo: Todo) {
        let alertController = UIAlertController(title: "「\(todo.title)」を削除します。", message: "よろしいですか?", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "キャンセル", style: .cancel))
        let deleteAction = UIAlertAction(title: "OK", style: .default) { _ in
            self.deleteTodo(id: todo.id)
        }
        alertController.addAction(deleteAction)
        present(alertController, animated: true)
    }
}

extension TodoListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return todos.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // セルを取得
        let cell = tableView.dequeueReusableCell(withIdentifier: "TodoCell", for: indexPath)
        // セルの値を設定
        cell.textLabel?.text = todos[indexPath.row].title
        return cell
    }
}

extension TodoListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let todo = todos[indexPath.row]
        tableView.deselectRow(at: indexPath, animated: true)
        if isDeleteMode {
            showDeleteAlert(todo: todo)
        } else {
            transitionToTodoEditView(todo: todo)
        }
    }
}

extension TodoListViewController: TodoEditViewControllerDelegate {
    func todoEditViewControllerDidEditTodo(_ todoEditViewController: TodoEditViewController, message: String) {
        navigationController?.popViewController(animated: true)
        showSnackbar(text: message)
        fetchTodoList()
    }
}
