//
//  RequestProtocol.swift
//  TodoApp
//
//  Created by rogiso on 2020/06/15.
//  Copyright © 2020 rogiso. All rights reserved.
//

import Alamofire
import Foundation

protocol RequestProtocol {
    associatedtype Response: Codable
    var baseUrl: String { get }
    var path: String { get }
    var method: HTTPMethod { get }
    var encoding: ParameterEncoding { get }
    var parameters: Parameters? { get }
    var headers: HTTPHeaders? { get }
}

extension RequestProtocol {
    var baseUrl: String {
        return "https://sonix-todo-rogiso.herokuapp.com"
    }
    var encoding: ParameterEncoding {
        return JSONEncoding.default
    }
    var headers: HTTPHeaders? {
        return ["Content-Type": "application/json"]
    }
}
