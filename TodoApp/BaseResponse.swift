//
//  BaseResponse.swift
//  TodoApp
//
//  Created by rogiso on 2020/06/18.
//  Copyright © 2020 rogiso. All rights reserved.
//

import Foundation

protocol BaseResponse: Codable {
    var errorCode: Int { get }
    var errorMessage: String { get }
}
