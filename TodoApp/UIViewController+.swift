//
//  UIViewController+.swift
//  TodoApp
//
//  Created by rogiso on 2020/06/26.
//  Copyright © 2020 rogiso. All rights reserved.
//

import UIKit

extension UIViewController {
    func showErrorAlert(message: String) {
        let alertController = UIAlertController(title: "エラー", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default))
        present(alertController, animated: true)
  }
}
