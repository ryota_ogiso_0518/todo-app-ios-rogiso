//
//  TodoEditViewController.swift
//  TodoApp
//
//  Created by rogiso on 2020/06/09.
//  Copyright © 2020 rogiso. All rights reserved.
//

import UIKit

protocol TodoEditViewControllerDelegate: class {
    func todoEditViewControllerDidEditTodo(_ todoEditViewController: TodoEditViewController, message: String)
}

final class TodoEditViewController: UIViewController {
    weak var delegate: TodoEditViewControllerDelegate?

    var todo: Todo?
    private let titleTextMaxCount = 100
    private let detailTextMaxCount = 1000
    private var isTitleLengthMaxOver = false
    private var isDetailLengthMaxOver = false
    private let datePicker = UIDatePicker()
    private let dateFormatter = DateFormatter()

    @IBOutlet private weak var registrationButton: UIButton!
    @IBOutlet private weak var detailTextView: UITextView!
    @IBOutlet private weak var detailCountLabel: UILabel!
    @IBOutlet private weak var titleTextField: UITextField!
    @IBOutlet private weak var titleCountLabel: UILabel!
    @IBOutlet private weak var dateTextField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy/M/d"
        setUpTextView()
        setUpNavigationItem()
        setUpRegistrationButton()
        setUpDatePicker()
        if let todo = todo {
            setText(todo: todo)
            loadTextCount()
        }
        switchRegistrationButtonMode()
    }

    private func setUpDatePicker() {
        datePicker.datePickerMode = .date
        datePicker.timeZone = NSTimeZone.local
        datePicker.locale = .current
        datePicker.calendar = Calendar(identifier: .gregorian)

        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 35))
        let spaceItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneItem = UIBarButtonItem(title: "決定", style: .done, target: self, action: #selector(didDoneButtonTapped))
        let deleteButtonItem = UIBarButtonItem(title: "削除", style: .done, target: self, action: #selector(didDeleteButtonTapped))
        let closeButtonItem = UIBarButtonItem(title: "閉じる", style: .done, target: self, action: #selector(didCloseButtonTapped))
        toolbar.setItems([deleteButtonItem, spaceItem, closeButtonItem, doneItem], animated: true)

        dateTextField.inputView = datePicker
        dateTextField.inputAccessoryView = toolbar
    }

    private func setUpNavigationItem() {
        navigationItem.title = "TODO APP"
    }

    private func setUpTextView() {
        detailTextView.layer.borderColor = UIColor.gray.cgColor
        detailTextView.layer.borderWidth = 1.0
        detailTextView.layer.cornerRadius = 10.0
        detailTextView.layer.masksToBounds = true
        detailTextView.delegate = self
    }

    @objc private func didDoneButtonTapped() {
        dateTextField.endEditing(true)
        dateTextField.text = dateFormatter.string(from: datePicker.date)
    }

    @objc private func didDeleteButtonTapped() {
        dateTextField.endEditing(true)
        dateTextField.text = ""
    }

    @objc private func didCloseButtonTapped() {
        dateTextField.endEditing(true)
    }

    @IBAction private func didTextFieldChanged(_ sender: Any) {
        guard let titleCount = titleTextField.text?.count else {
            return
        }
        titleCountLabel.text = String(titleCount)

        isTitleLengthMaxOver = titleCount > titleTextMaxCount
        titleCountLabel.textColor = isTitleLengthMaxOver ? .red : .black

        switchRegistrationButtonMode()
    }

    private func loadTextCount() {
        guard let titleCount = titleTextField.text?.count else {
            return
        }
        titleCountLabel.text = String(titleCount)
        detailCountLabel.text = String(detailTextView.text.count)
    }

    private func registerTodo(title: String, detail: String?, date: Date?) {
        APIClient().call(
            request: TodoPostRequest(title: title, detail: detail, date: date),
            success: { [weak self] _ in
                guard let self = self else { return }
                self.delegate?.todoEditViewControllerDidEditTodo(self, message: "登録しました")
            },
            failure: { [weak self] message in
                // 取得失敗時の処理
                self?.showErrorAlert(message: message)
            }
        )
    }

    private func updateTodo(id: Int, title: String, detail: String?, date: Date?) {
        let todo = Todo(id: id, title: title, detail: detail, date: date)
        APIClient().call(
            request: TodoUpdateRequest(todo: todo),
            success: { [weak self] _ in
                guard let self = self else { return }
                self.delegate?.todoEditViewControllerDidEditTodo(self, message: "更新しました")
            },
            failure: { [weak self] message in
                self?.showErrorAlert(message: message)
            }
        )
    }

    private func setText(todo: Todo) {
        titleTextField.text = todo.title
        detailTextView.text = todo.detail
        if let date = todo.date {
            dateTextField.text = dateFormatter.string(from: date)
        }
    }

    private func switchRegistrationButtonMode() {
        let isTitleTextEmpty = titleTextField.text?.isEmpty == true

        registrationButton.isEnabled = !isTitleLengthMaxOver && !isDetailLengthMaxOver && !isTitleTextEmpty
        registrationButton.backgroundColor = registrationButton.isEnabled ? .blue : .gray
    }

    private func setUpRegistrationButton() {
        if todo != nil {
            registrationButton.setTitle("更新", for: .normal)
        }
        registrationButton.backgroundColor = .blue
        registrationButton.layer.borderWidth = 0.5
        registrationButton.layer.borderColor = UIColor.black.cgColor
        registrationButton.layer.cornerRadius = 10.0
        registrationButton.setTitleColor(.white, for: .normal)
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    @IBAction private func onClickRegistrationButton(_ sender: Any) {
        guard let title = titleTextField.text else { return }
        let detail = detailTextView.text.isEmpty ? nil : detailTextView.text
        let dateText = dateTextField.text ?? ""
        let date = dateFormatter.date(from: dateText)
        if let id = todo?.id {
            updateTodo(id: id, title: title, detail: detail, date: date)
        } else {
            registerTodo(title: title, detail: detail, date: date)
        }
    }
}

extension TodoEditViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        let detailCount = detailTextView.text.count
        detailCountLabel.text = String(detailCount)

        isDetailLengthMaxOver = detailCount > detailTextMaxCount
        detailCountLabel.textColor = isDetailLengthMaxOver ? .red : .black

        switchRegistrationButtonMode()
    }
}
