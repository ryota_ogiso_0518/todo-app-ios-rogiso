//
//  APIClient.swift
//  TodoApp
//
//  Created by rogiso on 2020/06/15.
//  Copyright © 2020 rogiso. All rights reserved.
//

import Foundation
import Alamofire

final class APIClient {
    private let unexpectedErrorMessage = "予期せぬエラーが発生しました"
    private var commonDecorder: JSONDecoder {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        decoder.dateDecodingStrategy = .formatted(formatter)
        return decoder
    }

    func call<T: RequestProtocol>(request: T, success: @escaping (T.Response) -> Void, failure: @escaping (String) -> Void) {
        let requestUrl = request.baseUrl + request.path

        AF.request(requestUrl,
                   method: request.method,
                   parameters: request.parameters,
                   encoding: request.encoding,
                   headers: request.headers
        )
            .validate(statusCode: 200..<300)
            .responseData { response in
                guard let data = response.data else {
                    failure(self.unexpectedErrorMessage)
                    return
                }

                switch response.result {
                case .success:
                    do {
                        let result = try self.commonDecorder.decode(T.Response.self, from: data)
                        success(result)
                    } catch {
                        failure(self.unexpectedErrorMessage)
                    }
                case .failure:
                    let result = try? self.commonDecorder.decode(CommonResponse.self, from: data)
                    let message = result?.errorMessage ?? self.unexpectedErrorMessage
                    failure(message)
                }
        }
    }
}
