//
//  TodosGetResponse.swift
//  TodoApp
//
//  Created by rogiso on 2020/06/17.
//  Copyright © 2020 rogiso. All rights reserved.
//

import Foundation

struct TodosGetResponse: BaseResponse {
    var todos: [Todo]
    var errorCode: Int
    var errorMessage: String
}
