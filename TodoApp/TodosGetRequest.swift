//
//  TodosGetRequest.swift
//  TodoApp
//
//  Created by rogiso on 2020/06/16.
//  Copyright © 2020 rogiso. All rights reserved.
//

import Alamofire

struct TodosGetRequest: RequestProtocol {
    typealias Response = TodosGetResponse 

    var parameters: Parameters? {
        return nil
    }
    var path: String {
        return "/todos"
    }
    var method: HTTPMethod {
        return .get
    }
}
