//
//  TodoDeleteRequestTests.swift
//  TodoAppTests
//
//  Created by rogiso on 2020/07/31.
//  Copyright © 2020 rogiso. All rights reserved.
//

import XCTest
import OHHTTPStubs
@testable import TodoApp

final class TodoDeleteRequestTests: XCTestCase {
    override func tearDown() {
        HTTPStubs.removeAllStubs()
        super.tearDown()
    }

    func testInit() {
        let request = TodoDeleteRequest(id: 1)

        XCTAssertEqual(request.method, .delete)
        XCTAssertEqual(request.path, "/todos/1")
    }

    func testResponse() {
        var todoDeleteResponse: CommonResponse?

        stub(condition: isHost("sonix-todo-rogiso.herokuapp.com")) { _ in
            return HTTPStubsResponse(
                fileAtPath: OHPathForFile("CommonResponse.json", type(of: self))!,
                statusCode: 200,
                headers: ["Content-Type": "application/json"]
            )
        }

        let expectation = self.expectation(description: "Delete Todo Request")

        APIClient().call(
            request: TodoDeleteRequest(id: 1),
            success: { response in
                todoDeleteResponse = response
                expectation.fulfill()
        }, failure: { _ in
            return
        })

        waitForExpectations(timeout: 10) { _ in
            XCTAssertNotNil(todoDeleteResponse)
            XCTAssertEqual(todoDeleteResponse?.errorCode, 0)
            XCTAssertEqual(todoDeleteResponse?.errorMessage, "")
        }
    }
}
