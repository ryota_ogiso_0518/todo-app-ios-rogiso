//
//  TodoPostRequestTests.swift
//  TodoAppTests
//
//  Created by rogiso on 2020/08/03.
//  Copyright © 2020 rogiso. All rights reserved.
//

import XCTest
import OHHTTPStubs
@testable import TodoApp

final class TodoPostRequestTests: XCTestCase {
    override func tearDown() {
        HTTPStubs.removeAllStubs()
        super.tearDown()
    }

    func testInit() {
        let request = TodoPostRequest(title: "test4", detail: nil, date: nil)

        XCTAssertEqual(request.method, .post)
        XCTAssertEqual(request.path, "/todos")
    }

    func testResponse() {
        var todoPostResponse: CommonResponse?

        stub(condition: isHost("sonix-todo-rogiso.herokuapp.com")) { _ in
            return HTTPStubsResponse(
                fileAtPath: OHPathForFile("CommonResponse.json", type(of: self))!,
                statusCode: 200,
                headers: ["Content-Type": "application/json"]
            )
        }

        let expectation = self.expectation(description: "Post Todo Request")

        APIClient().call(
            request: TodoPostRequest(title: "test4", detail: nil, date: nil),
            success: { response in
                todoPostResponse = response
                expectation.fulfill()
        }, failure: { _ in
            return
        })
        
        waitForExpectations(timeout: 10) { _ in
            XCTAssertNotNil(todoPostResponse)
            XCTAssertEqual(todoPostResponse?.errorCode, 0)
            XCTAssertEqual(todoPostResponse?.errorMessage, "")
        }
    }
}
