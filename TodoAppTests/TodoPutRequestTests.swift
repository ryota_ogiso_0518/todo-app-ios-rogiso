//
//  TodoPutRequestTests.swift
//  TodoAppTests
//
//  Created by rogiso on 2020/08/03.
//  Copyright © 2020 rogiso. All rights reserved.
//

import XCTest
import OHHTTPStubs
@testable import TodoApp

final class TodoPutRequestTests: XCTestCase {
    let todo = Todo(id: 1, title: "更新テスト", detail: nil, date: nil)
    
    override func tearDown() {
        HTTPStubs.removeAllStubs()
        super.tearDown()
    }
    
    func testInit(){
        let request = TodoUpdateRequest(todo: todo)
        
        XCTAssertEqual(request.method, .put)
        XCTAssertEqual(request.path, "/todos/1")
    }
    
    func testResponse() {
        var todoPutResponse: CommonResponse?
        
        stub(condition: isHost("sonix-todo-rogiso.herokuapp.com")) { _ in
            return HTTPStubsResponse(
                fileAtPath: OHPathForFile("CommonResponse.json", type(of: self))!,
                statusCode: 200,
                headers: ["Content-Type": "application/json"]
            )
        }
        
        let expectation = self.expectation(description: "Put Todo Request")
        
        APIClient().call(
            request: TodoUpdateRequest(todo: todo),
            success: { response in
                todoPutResponse = response
                expectation.fulfill()
        }, failure: { _ in
            return
        })
        
        waitForExpectations(timeout: 10) { _ in
            XCTAssertNotNil(todoPutResponse)
            XCTAssertEqual(todoPutResponse?.errorCode, 0)
            XCTAssertEqual(todoPutResponse?.errorMessage, "")
        }
    }
}
